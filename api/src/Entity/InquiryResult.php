<?php

namespace App\Entity;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * description="InquiryResult model",
 * type="object",
 * title="InquiryResult"
 * )
 */
class InquiryResult
{
    /**
     * @var string
     * @OA\Property(
     *     property="status",
     *     type="string",
     *     example="success"
     * )
     */
    public $status;

    /**
     * @var string
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     example="No errors"
     * )
     */
    public $message;
}