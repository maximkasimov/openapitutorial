<?php

namespace App\Entity;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * description="Inquiry model",
 * type="object",
 * title="Inquiry"
 * )
 */
class Inquiry
{
    /**
     * @var string
     * @OA\Property(
     *     property="firstname",
     *     type="string",
     *     example="Maxim"
     * )
     */
    public $firstname;

    /**
     * @var string
     * @OA\Property(
     *     property="city",
     *     type="string",
     *     example="Kyiv"
     * )
     */
    public $city;

    /**
     * @var string
     * @OA\Property(
     *     property="phone",
     *     type="string",
     *     example="+380123456789"
     * )
     */
    public $phone;

    /**
     * @var string
     * @OA\Property(
     *     property="email",
     *     type="string",
     *     example="maxim@example.com"
     * )
     */
    public $email;
}