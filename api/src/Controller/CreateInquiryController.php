<?php

namespace App\Controller;
use App\Entity\Inquiry;
use App\Entity\InquiryResult;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

class CreateInquiryController extends AbstractController
{
    /**
     * @Route("/api/create_inquiry", name="create_inquiry", methods={"POST"})
     * @OA\Post(
     *     path="/api/create_inquiry",
     *     summary="Create a new inquiry",
     *     @OA\RequestBody(
     *         @OA\JsonContent(ref=@Model(type=Inquiry::class))
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Request result",
     *        @OA\JsonContent(ref=@Model(type=InquiryResult::class))
     *     )
     * )
     * @OA\Tag(name="CreateInquiry")
     */
    public function index(Request $request): Response
    {
        $params = array();
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $params = json_decode($request->getContent(), true);
        }

        return $this->json([
            'status' => 'success',
            'message' => 'No errors',
        ]);
    }
}
